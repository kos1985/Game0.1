﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitBarriers : MonoBehaviour {

    public GameObject[] barriersLeft = new GameObject[5]; //массив точек для барьеров слева
    public GameObject[] barriersRight = new GameObject[5]; //массив точек для барьеров справа

    public GameObject barrierLeft; //префаб левого барьера
    public GameObject barrierRight; //префаб правого барьера

    //private Random rand;

    void Start () {

        // Проходим по массиву точек для левых барьеров 
        // и случайным образом инициализируем для каждой точки барьер 
        foreach (GameObject left in barriersLeft)
        {
            if (Random.Range(0, 2) == 1)
            {
                barrierLeft.transform.position = left.transform.position;
                Instantiate(barrierLeft);
            }
        }

        // Проходим по массиву точек для правых барьеров 
        // и случайным образом инициализируем для каждой точки барьер 
        foreach (GameObject right in barriersRight)
        {
            if (Random.Range(0, 2) == 1)
            {
                barrierRight.transform.position = right.transform.position;
                Instantiate(barrierRight);
            }
        }





    }
	
	
	void Update () {
		
	}
}
