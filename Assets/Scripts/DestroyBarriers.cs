﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBarriers : MonoBehaviour {

    private GameObject player;

    
    void Start () {

        player = GameObject.FindGameObjectWithTag("Player");

	}
	
	
	void Update () {

        //уничтожаем барьер после того как окажется за игроком
        if (transform.position.z < (player.transform.position.z - 5))
        {

            Destroy(this.gameObject);

        }

    }
}
