﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateRoad : MonoBehaviour
{

    public GameObject road;

    void Start()
    {

    }


    void Update()
    {

        //удаление лишних префабов дороги
        GameObject[] _roads;
        _roads = GameObject.FindGameObjectsWithTag("Road");
        if (_roads.Length > 3)
        {
            Destroy(_roads[0]);
        }

        //GameObject[] _barriers;
        //_barriers = GameObject.FindGameObjectsWithTag("Barrier");
        //if (_barriers.Length > 30)
        //{
        //    for (int i = 0; i <= 10; i++)
        //    {
        //        Destroy(_barriers[i]);
        //    }

        //}



    }

    //если заходит в триггер, появляется новая дорога
    void OnTriggerEnter(Collider col)
    {
        float z = transform.position.z; 

        if (col.tag == "Player")
        {
            Instantiate(road, new Vector3(0, 0, z + 50), Quaternion.identity);

        }
    }

}
