﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscPaused : MonoBehaviour {

    bool isPaused = false; // Стоит ли игра на паузе


	void Update () {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!isPaused)
            {
                Time.timeScale = 0;
                isPaused = true;
            }
            else {
                Time.timeScale = 1;
                isPaused = false;
            }
        }


	}
}
