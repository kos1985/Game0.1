﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    //-1,5 и 1,5

    private int pointer; //указатель стороны
    //public bool keyInput; // указатель нажатия клавиши
    //private float speedMove; //скорость передвижения

    void Start () {

        //Задаем начальные значения скорости и указателю стороны
        pointer = 1;
        //speedMove = 0.5f;

    }
	
	
	void Update () {

        /*//Если нажата клавиша присваеваем указателю нажатия TRUE
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            keyInput = true;
        }

        //Если указатель нажатия TRUE, то начинаем движение
        if (keyInput)
        {
            transform.Translate(Vector3.forward * speedMove);
        }*/

        //Переменные координаты в данный момент
        float x = transform.position.x;
        float y = transform.position.y;
        float z = transform.position.z;


        //Смена стороны по нажатию Пробела
        if (Input.GetKeyDown(KeyCode.Space))
        {
            x = 1.5f * pointer;
            transform.position = new Vector3(x, y, z);
            pointer = (-1) * pointer;
        }

        


    }
}
